<?php

namespace DreamCat\FrameHtmlCtl;

/**
 * html控制器的输出
 * @author vijay
 */
class HtmlControllerResponse
{
    /** @var string 模板文件路径 */
    private $file;
    /** @var array 数据 */
    private $data;

    /**
     * @return string 模板文件路径
     */
    public function getFile(): string
    {
        return $this->file;
    }

    /**
     * @param string $file 模板文件路径
     * @return static 对象本身
     */
    public function setFile(string $file): HtmlControllerResponse
    {
        $this->file = $file;
        return $this;
    }

    /**
     * @return array 数据
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * @param array $data 数据
     * @return static 对象本身
     */
    public function setData(array $data): HtmlControllerResponse
    {
        $this->data = $data;
        return $this;
    }
}

# end of file
