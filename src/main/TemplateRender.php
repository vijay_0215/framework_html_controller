<?php

namespace DreamCat\FrameHtmlCtl;

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;
use Twig\Loader\FilesystemLoader;

/**
 * lex 渲染器
 * @author vijay
 */
class TemplateRender
{
    /**
     * @Config template/dir
     * @var string 模板根目录
     */
    private $templateDir = "templates";
    /**
     * @Config template/cache
     * @var string 模板缓存目录
     */
    private $templateCache = "cache/templates";
    /**
     * @RootPath
     * @var string 项目根目录
     */
    private $rootDir = "";

    /** @var Environment twig环境 */
    private $twig;

    /**
     * @return string 模板缓存目录
     */
    public function getTemplateCache(): string
    {
        return $this->templateCache;
    }

    /**
     * @param string $templateCache 模板缓存目录
     * @return TemplateRender
     */
    public function setTemplateCache(string $templateCache): TemplateRender
    {
        $this->templateCache = $templateCache;
        return $this;
    }

    /**
     * @return string 项目根目录
     */
    public function getRootDir(): string
    {
        return $this->rootDir;
    }

    /**
     * @param string $rootDir 项目根目录
     * @return TemplateRender
     */
    public function setRootDir(string $rootDir): TemplateRender
    {
        $this->rootDir = $rootDir;
        return $this;
    }

    /**
     * @return Environment twig环境
     */
    public function getTwig(): Environment
    {
        if (!$this->twig) {
            $this->twig = new Environment(
                new FilesystemLoader("{$this->getRootDir()}/{$this->getTemplateDir()}"),
                ["cache" => $this->getTemplateCache()]
            );
        }
        return $this->twig;
    }

    /**
     * @param Environment $twig twig环境
     * @return TemplateRender
     */
    public function setTwig(Environment $twig): TemplateRender
    {
        $this->twig = $twig;
        return $this;
    }

    /**
     * @return string 模板根目录
     */
    public function getTemplateDir(): string
    {
        return $this->templateDir;
    }

    /**
     * @param string $templateDir 模板根目录
     * @return TemplateRender
     */
    public function setTemplateDir(string $templateDir): TemplateRender
    {
        $this->templateDir = $templateDir;
        return $this;
    }

    /**
     * 渲染页面
     * @param HtmlControllerResponse $response html控制器的输出
     * @return string html页面
     * @throws LoaderError 模板文件找不到的时候
     * @throws RuntimeError 当先前生成的缓存损坏时
     * @throws SyntaxError 编译出现语法错误的时候
     */
    public function parse(HtmlControllerResponse $response): string
    {
        return $this->getTwig()->load($response->getFile())->render($response->getData());
    }
}

# end of file
