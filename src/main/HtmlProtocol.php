<?php

namespace DreamCat\FrameHtmlCtl;

use DreamCat\FrameInterface\Controller\ProtocolInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;
use Zend\Diactoros\Response\HtmlResponse;

/**
 * 前后端未分离的模板协议
 * @author vijay
 */
class HtmlProtocol implements ProtocolInterface
{
    /**
     * @Autowire
     * @var TemplateRender
     */
    private $render;

    /**
     * 输入转换
     * @param ServerRequestInterface $request 请求对象
     * @return ServerRequestInterface 转换后的请求对象
     */
    public function convertInput(ServerRequestInterface $request): ServerRequestInterface
    {
        return $request;
    }

    /**
     * 输出转换
     * @param ResponseInterface $response 响应消息
     * @return ResponseInterface 响应消息
     */
    public function convertOutput(ResponseInterface $response): ResponseInterface
    {
        return $response;
    }

    /**
     * 格式化控制器方法的输出值
     * @param mixed $response 控制器方法返回值
     * @return ResponseInterface 标准响应消息
     * @throws LoaderError  模板文件找不到的时候
     * @throws RuntimeError 当先前生成的缓存损坏时
     * @throws SyntaxError  编译出现语法错误的时候
     */
    public function formatOutput($response): ResponseInterface
    {
        if ($response instanceof HtmlControllerResponse) {
            return new HtmlResponse($this->render->parse($response));
        }
        throw new \RuntimeException("控制器返回的响应消息不合法");
    }
}

# end of file
