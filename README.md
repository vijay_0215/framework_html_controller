# framework_html_controller

## 介绍
用于喵框架的输出html的协议实现

## 安装教程
```shell script
composer require dreamcat/frame_html_controller
```

## 使用方法
控制器使用 `DreamCat\FrameHtmlCtl\HtmlProtocol` 类做协议即可，要求控制器方法返回 `DreamCat\FrameHtmlCtl\HtmlControllerResponse` ，其中的数组，
键是模板中的变量名称，支持多级数组，内部使用 [Twig](https://twig.symfony.com)。

